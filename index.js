const createImageObject = require('./lib/createImageObject');
const isAnime = require('./lib/isAnime');
const isManga = require('./lib/isManga');
const limits = require('./lib/limits');
const mergeArray = require('./lib/mergeArray');
const searchObjectInArray = require('./lib/searchObjectInArray');
const shuffleArray = require('./lib/shuffleArray');

module.exports = {
  createImageObject,
  isAnime,
  isManga,
  limits,
  mergeArray,
  searchObjectInArray,
  shuffleArray,
};
