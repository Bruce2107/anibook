function isManga(object) {
  return 'whereRead' in object;
}

module.exports = isManga;
