function isAnime(object) {
  return 'whereWatch' in object;
}

module.exports = isAnime;
